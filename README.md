# Gameskii Technical Test

## Getting started

Please following this steps:
- Create a branch with format <b>":name:-gameskii-test"</b> Example: rifki-gameskii-test.
- Create folder with your name on root directory Example: rifki.
- Add your changes inside of your folder with commit title <b>"Assesment Gameskii"</b>.
- After the task is running well, you can create the merge request to the ```main``` branch with format <b>":name: Assesment"</b>, Example: "Rifki Assesment".

## Acceptance Criteria

- Code must be written using Go Language.
- Build with a database like a MySQL or PostgreSQL to store the data.
- Feel free to create model data, design of database and code pattern. We will collection your point base on how clean the code etc.

## Task
Check on the ```task.jpg```, you will see note of order on a supermarket. Your task is build a api to store the data of products and order.

### Goal:
- Provides a API that return data of orders by following mock bellow.
```
    {
        "total_amount": 98000,
        "charge": 100000,
        "return": 2000,
        "products": [
            {
                "name": "Pencil",
                "quantity": 2,
                "amount": 4000,
                "total_amount": 8000
            },
            {
                "name": "Book",
                "quantity": 4,
                "amount": 10000,
                "total_amount": 40000
            },
            {
                "name": "Bag",
                "quantity": 1,
                "amount": 50000,
                "total_amount": 50000
            }
        ],
        "created_at": "2023-01-16T15:04:05Z"
    }
```
- Provide a API to create a new data order.